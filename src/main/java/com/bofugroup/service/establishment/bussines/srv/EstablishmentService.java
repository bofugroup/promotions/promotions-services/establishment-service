package com.bofugroup.service.establishment.bussines.srv;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bofugroup.service.establishment.business.dto.Establishment;
import com.bofugroup.service.establishment.exception.ExcepctionEstablishmentNotFound;
import com.bofugroup.service.establishment.repository.EstablishmentRepository;

@Component
public class EstablishmentService 
{
	@Autowired
	EstablishmentRepository repository;
	
	public List<Establishment> all()
	{
		return (List<Establishment>) repository.findAll();
	}
	
	public Optional<Establishment> getId(Long id)
	{
		return repository.findById(id);
	}
	
	public Establishment save(Establishment establisment)
	{
		Establishment response = null;
		if(establisment != null) 
			response = repository.save(establisment);
		else
			throw new ExcepctionEstablishmentNotFound();
		
		return response;
	}
}
