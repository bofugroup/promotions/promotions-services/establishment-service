package com.bofugroup.service.establishment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.establishment.business.dto.Establishment;

@Repository
public interface EstablishmentRepository  extends CrudRepository<Establishment, Long>
{

}
