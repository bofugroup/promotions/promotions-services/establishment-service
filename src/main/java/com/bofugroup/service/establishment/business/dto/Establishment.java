package com.bofugroup.service.establishment.business.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="establishment_tbl")
public class Establishment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The database generated product ID")
	Long id;
	@ApiModelProperty(notes = "the name of establishment")
	@Column(name="name",nullable=false)
	String name;
	@ApiModelProperty(notes = "the long description of establishment")
	@Column(name="description",nullable=false)
	String description;
	@ApiModelProperty(notes = "the location of establishment")
	@Column(name="address",nullable=false)
	String address;
	@ApiModelProperty(notes = "the contacted phone of establishment")
	@Column(name="phone",nullable=false)
	String phone;
	@ApiModelProperty(notes = "url of establishment example: youtube, facebook.com")
	@Column(name="url",nullable=true)
	String url;
	@ApiModelProperty(notes = "number of stars of amwesome")
	@Column(name="stars",nullable=true)
	int stars;
	

	public Establishment() {}

	
	public Establishment(String name, String description, String address, String phone, String url, int stars) {
		super();
		this.name = name;
		this.description = description;
		this.address = address;
		this.phone = phone;
		this.url = url;
		this.stars = stars;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

}
