package com.bofugroup.service.establishment.facade.mapper.Imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bofugroup.service.establishment.business.dto.Establishment;
import com.bofugroup.service.establishment.facade.dto.DTOinEstablishment;
import com.bofugroup.service.establishment.facade.dto.DTOoutEstablishment;
import com.bofugroup.service.establishment.facade.mapper.IEstablishmentMapper;


@Component("EstablishmentMapper")
public class EstablishmentMapper implements IEstablishmentMapper
{
	@Override
	public List<DTOoutEstablishment> mapOutListEstablishment(List<Establishment> establishments) 
	{
		List<DTOoutEstablishment> response = new ArrayList<DTOoutEstablishment>();
		
		if(establishments != null && !establishments.isEmpty())
		{
			for(Establishment temp : establishments)
			{
				response.add(mapOutEstablishment(temp));
			}
		}
		return response;
	}
	
	
	@Override
	public DTOoutEstablishment mapOutEstablishment(Establishment establishment) 
	{
		DTOoutEstablishment response = new DTOoutEstablishment();
		if(establishment != null) 
		{
			response.setId(establishment.getId());
			response.setName(establishment.getName());
			response.setAddress(establishment.getAddress());
			response.setDescription(establishment.getDescription());
			response.setPhone(establishment.getPhone());
			response.setUrl(establishment.getUrl());
			response.setStars(establishment.getStars());
		}
		
		return response;
	}

	@Override
	public Establishment mapInEstablishment(DTOinEstablishment dtoInEstablishment) 
	{
		Establishment establishment = new Establishment();
		if(dtoInEstablishment != null) 
		{
			establishment.setName(dtoInEstablishment.getName());
			establishment.setAddress(dtoInEstablishment.getAddress());
			establishment.setDescription(dtoInEstablishment.getDescription());
			establishment.setPhone(dtoInEstablishment.getPhone());
			establishment.setUrl(dtoInEstablishment.getUrl());
			establishment.setStars(dtoInEstablishment.getStars());
		}
			return establishment;
	}

}
