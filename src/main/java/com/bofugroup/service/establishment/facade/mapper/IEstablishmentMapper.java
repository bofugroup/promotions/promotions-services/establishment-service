package com.bofugroup.service.establishment.facade.mapper;

import java.util.List;

import com.bofugroup.service.establishment.business.dto.Establishment;
import com.bofugroup.service.establishment.facade.dto.DTOinEstablishment;
import com.bofugroup.service.establishment.facade.dto.DTOoutEstablishment;

public interface IEstablishmentMapper 
{
	DTOoutEstablishment mapOutEstablishment(Establishment establishment);
	Establishment mapInEstablishment(DTOinEstablishment dtoInEstablishment); 
	List<DTOoutEstablishment> mapOutListEstablishment(List<Establishment> establishment);
}
