package com.bofugroup.service.establishment.facade.Imp;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bofugroup.service.establishment.bussines.srv.EstablishmentService;
import com.bofugroup.service.establishment.facade.IEstablishmentController;
import com.bofugroup.service.establishment.facade.dto.DTOinEstablishment;
import com.bofugroup.service.establishment.facade.dto.DTOoutEstablishment;
import com.bofugroup.service.establishment.facade.mapper.IEstablishmentMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/establishment")
@RestController
@Api(value="Establishment Resource" ,produces = "application/json")
public class EstablishmentController implements IEstablishmentController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EstablishmentController.class);
	
	@Autowired
	IEstablishmentMapper mapper;
	
	@Autowired
	EstablishmentService establishmentService;

	@ApiOperation(value = "View a list of available establishments", response = ArrayList.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	@RequestMapping(value = "", method= RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<List<DTOoutEstablishment>> all()
	{
		List<DTOoutEstablishment> establishments = new ArrayList<>();
		LOGGER.info("init return list of establishment");
		establishments = mapper.mapOutListEstablishment(establishmentService.all());
		LOGGER.info("return list of establishment");
		return new ResponseEntity<List<DTOoutEstablishment>>(establishments, HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "save a establishment in db", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully save in bd"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	@RequestMapping(value = "", method= RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> add(@RequestBody DTOinEstablishment dtoInEstablishment)
	{
		DTOoutEstablishment response = null;
		response = mapper.mapOutEstablishment(establishmentService.save(
												  mapper.mapInEstablishment(dtoInEstablishment)));	
		if(response != null)
		{
			URI location = ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(response.getId())
					.toUri();
			
			return ResponseEntity.created(location).build();
		}
		else
		{
			LOGGER.error("has errors establishment");
			return ResponseEntity.noContent().build();
		}	
	}
	
	
	@ApiOperation(value = "save a establishment in db", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "establishment found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "bad request"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	@RequestMapping(value = "/{establishmentId}", method= RequestMethod.GET, produces = "application/json")
	public ResponseEntity<DTOoutEstablishment> findByEstablisment(@PathVariable("establishmentId") Long establishmentId)
	{
		LOGGER.error("has errors establishment");
		return new ResponseEntity<DTOoutEstablishment>(new DTOoutEstablishment(), HttpStatus.OK);
	}
}
