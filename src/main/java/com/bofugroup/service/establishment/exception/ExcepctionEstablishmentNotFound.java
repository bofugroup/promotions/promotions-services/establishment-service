package com.bofugroup.service.establishment.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="not found Establishment")
public class ExcepctionEstablishmentNotFound extends RuntimeException
{
	private static final long serialVersionUID = 1L;
}
